#pragma once
#include "JsonData.h"

class JsonIO
{
private:
	vector<JsonData*> vecJsonData;
	map<int, JsonData*> mapJsonData;
	unordered_map<int, JsonData*> hashMapJsonData;
	list<JsonData*> listJsonData;

public:
	JsonIO();
	~JsonIO();

	void writeJson(const string fileName, const int objNum);
	void loadJson(const string fileName);

	vector<JsonData*> getVecJsonData() const;
	map<int, JsonData*> getMapJsonData() const;
	unordered_map<int, JsonData*> getHashMapJsonData() const;
	list<JsonData*> getListJsonData() const;

	void addCmp(JsonData* jd, const int dataModel);
	void deleteCmp(int jdObjId, const int dataModel);
	void updateCmp(int jdObjId, const int dataModel);
	void findCmp(int jdObjId, const int dataModel);

	void cleanUp();
};

