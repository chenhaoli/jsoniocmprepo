#include "JsonData.h"

JsonData::JsonData(const int newObjId, const int newRandNum, const string newStrTime, const time_t newIntTime) {
	this->objId = newObjId;
	this->randNum = newRandNum;
	this->strTime = newStrTime;
	this->intTime = newIntTime;
	cout << "New JsonData Obj Success!" << endl;
}
JsonData::JsonData(JsonData* jd) {
	this->objId = jd->objId;
	this->randNum = jd->randNum;
	this->strTime = jd->strTime;
	this->intTime = jd->intTime;
	cout << "New JsonData Obj Success!" << endl;
}
JsonData::JsonData(){
	time_t t_ = time(NULL);
	this->objId = g_objId;
	this->intTime = t_;
	this->strTime = intToStr();
	this->randNum = rand();
	g_objId += 1;
	cout << "New JsonData Obj Success!" << endl;
}
JsonData::~JsonData() {
	cout << "Delete JsonData Obj Success!" << endl;
}

void JsonData::setObjId(const int newObjId) {
	this->objId = newObjId;
}
void JsonData::setRandNum(const int newRandNum) {
	this->randNum = newRandNum;
}
void JsonData::setStrTime(const string newStrTime) {
	this->strTime = newStrTime;
}
void JsonData::setIntTime(const time_t newIntTime) {
	this->intTime = newIntTime;
}

int JsonData::getObjId() const {
	return this->objId;
}
int JsonData::getRandNum() const {
	return this->randNum;
}
string JsonData::getStrTime() const {
	return this->strTime;
}
time_t JsonData::getIntTime() const {
	return this->intTime;
}

string JsonData::timeToStr(tm* _t) const {
	int year, month, day, hour, minute, second;
	year = _t->tm_year + 1900;
	month = _t->tm_mon + 1;
	day = _t->tm_mday;
	hour = _t->tm_hour;
	minute = _t->tm_min;
	second = _t->tm_sec;

	string temp, str;
	stringstream ss;
	ss << setw(4) << setfill('0') << year << "-" << setw(2) << setfill('0') << month << "-" << setw(2) << setfill('0') << day << " ";
	ss << setw(2) << setfill('0') << hour << ":" << setw(2) << setfill('0') << minute << ":" << setw(2) << setfill('0') << second;
	while (ss >> temp) {
		str = str + temp + " ";
	}

	ss.clear();
	return str;

	/*char strYear[5], strMon[3], strDay[3], strHour[3], strMin[3], strSec[3];
	sprintf(strYear, "%04d", year);
	sprintf(strMon, "%02d", month);
	sprintf(strDay, "%02d", day);
	sprintf(strHour, "%02d", hour);
	sprintf(strMin, "%02d", minute);
	sprintf(strSec, "%02d", second);

	char s[20];
	sprintf(s, "%s-%s-%s %s:%s:%s", strYear, strMon, strDay, strHour, strMin, strSec);
	string str(s);
	return str;*/
}

tm JsonData::strToTime() const {
	char* ch = (char*)strTime.data();
	tm tm_;
	int year, month, day, hour, minute, second;
	sscanf_s(ch, "%d-%d-%d %d:%d:%d", &year, &month, &day, &hour, &minute, &second);
	tm_.tm_year = year - 1900;
	tm_.tm_mon = month - 1;
	tm_.tm_mday = day;
	tm_.tm_hour = hour;
	tm_.tm_min = minute;
	tm_.tm_sec = second;
	tm_.tm_isdst = 0;

	return tm_;
}

string JsonData::intToStr() const {
	tm* tm_ = localtime(&intTime);
	string str = timeToStr(tm_);
	return str;
}
time_t JsonData::strToInt() const {
	tm tm_ = strToTime();
	time_t t_ = mktime(&tm_);
	
	return t_;
}

bool JsonData::operator==(int jdObjId) const {
	if (jdObjId == this->objId
/*		&& jd->getRandNum() == this->randNum
		&& jd->getStrTime() == this->strTime
		&& jd->getIntTime() == this->intTime*/)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//void JsonData::cleanUp() {
//	delete this;
//}