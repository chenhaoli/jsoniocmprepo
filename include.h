#pragma once
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <time.h>
#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <sstream>
#include <iomanip>


#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/istreamwrapper.h"

using namespace std;
using namespace rapidjson;
static int g_objId = 0;