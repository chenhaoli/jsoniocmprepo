#pragma once
#include "include.h"

class JsonData
{
private:
	int objId;
	int randNum;
	string strTime;
	time_t intTime;

public:
	JsonData(const int newObjId, const int newRandNum, const string newStrTime, const time_t newIntTime);
	JsonData(JsonData* jd);
	JsonData();
	~JsonData();

	void setObjId(const int newObjId);
	void setRandNum(const int newRandNum);
	void setStrTime(const string newStrTime);
	void setIntTime(const time_t newIntTime);

	int getObjId() const;
	int getRandNum() const;
	string getStrTime() const;
	time_t getIntTime() const;

	string timeToStr(tm* _t) const;
	tm strToTime() const;
	string intToStr() const;
	time_t strToInt() const;

	bool operator== (int jdObjId) const;

	//void cleanUp();
};