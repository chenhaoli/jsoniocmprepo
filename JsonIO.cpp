#include "JsonIO.h"

JsonIO::JsonIO() {
	cout << "New JsonIO Obj Success!" << endl;
}
JsonIO::~JsonIO() {
	cout << "Delete JsonIO Obj Success!" << endl;
}

void JsonIO::writeJson(const string fileName, const int objNum) {
	ofstream ofs(fileName, ios::out);
	ofs << "[\n";
	for (int i = 0; i < objNum; ++i) {
		JsonData* jd_ = new JsonData();
		ofs << "{" << "\"objId\":" << jd_->getObjId() << ",";
		ofs << "\"randNum\":" << jd_->getRandNum() << ",";
		ofs << "\"strTime\":\"" << jd_->getStrTime() << "\",";
		ofs << "\"intTime\":" << jd_->getIntTime() << "}";
		delete jd_;
		if (i < objNum - 1) {
			ofs << ",";
		}
		ofs << "\n";
	}
	ofs << "]";
	ofs.close();
	cout << "----------------------Write Json Success!----------------------" << endl;
}
void JsonIO::loadJson(const string fileName) {
	ifstream ifs(fileName, ios::in);
	IStreamWrapper isw(ifs);
	Document document;
	document.ParseStream(isw);
	ifs.close();
	for (int i = 0; i < document.Size(); ++i) {
		Value& v = document[i];
		
		vecJsonData.push_back(new JsonData(
			v["objId"].GetInt(),
			v["randNum"].GetInt(),
			v["strTime"].GetString(),
			v["intTime"].GetInt64()
		));
		mapJsonData.insert(make_pair(v["objId"].GetInt(), new JsonData(
			v["objId"].GetInt(),
			v["randNum"].GetInt(),
			v["strTime"].GetString(),
			v["intTime"].GetInt64()
		)));
		hashMapJsonData.insert(make_pair(v["objId"].GetInt(), new JsonData(
			v["objId"].GetInt(),
			v["randNum"].GetInt(),
			v["strTime"].GetString(),
			v["intTime"].GetInt64()
		)));
		listJsonData.push_back(new JsonData(
			v["objId"].GetInt(),
			v["randNum"].GetInt(),
			v["strTime"].GetString(),
			v["intTime"].GetInt64()
		));
	}
	cout << "----------------------Load JsonData Success!----------------------" << endl;
}

vector<JsonData*> JsonIO::getVecJsonData() const {
	return vecJsonData;
}
map<int, JsonData*> JsonIO::getMapJsonData() const {
	return mapJsonData;
}
unordered_map<int, JsonData*> JsonIO::getHashMapJsonData() const {
	return hashMapJsonData;
}
list<JsonData*> JsonIO::getListJsonData() const {
	return listJsonData;
}

void JsonIO::addCmp(JsonData* jd, const int dataModel) {
	//0:vector add
	//1:map add
	//2:hash_map add
	//3:list add
	switch (dataModel)
	{
	case 0:
		vecJsonData.push_back(jd);
		cout << "Vector Add JsonData ID:" << jd->getObjId() << " Success" << endl;
		break;
	case 1:
		mapJsonData.insert(make_pair(jd->getObjId(), jd));
		cout << "Map Add JsonData ID:" << jd->getObjId() << " Success" << endl;
		break;
	case 2:
		hashMapJsonData.insert(make_pair(jd->getObjId(), jd));
		cout << "HashMap Add JsonData ID:" << jd->getObjId() << " Success" << endl;
		break;
	case 3:
		listJsonData.push_back(jd);
		cout << "List Add JsonData ID:" << jd->getObjId() << " Success" << endl;
		break;
	default:
		break;
	}
}
void JsonIO::deleteCmp(int deleteNum, const int dataModel) {
	//0:vector delete
	//1:map delete
	//2:hash_map delete
	//3:list delete
	int count = 0;
	switch (dataModel)
	{
	case 0: {
		vector<JsonData*>::iterator vecItr = vecJsonData.begin();
		while (count <= deleteNum && vecItr != vecJsonData.end())
		{
			count++;
			vecItr = vecJsonData.erase(vecItr);
			cout << count << " Vector Delete Success!" << endl;
		}
		if (count < deleteNum) {
			cout << "Vector Obj Num is Not Enough!" << endl;
		}
	}
		break;
	case 1: {
		map<int, JsonData*>::iterator mapItr = mapJsonData.begin();
		while (count <= deleteNum && mapItr != mapJsonData.end())
		{
			count++;
			mapJsonData.erase(mapItr++);
			cout << count << " Map Delete Success!" << endl;
		}
		if (count < deleteNum) {
			cout << "Map Obj Num is Not Enough!" << endl;
		}
	}
		break;
	case 2: {
		unordered_map<int, JsonData*>::iterator hashMapItr = hashMapJsonData.begin();
		while (count <= deleteNum && hashMapItr != hashMapJsonData.end())
		{
			count++;
			hashMapJsonData.erase(hashMapItr++);
			cout << count << " HashMap Delete Success!" << endl;
		}
		if (count < deleteNum) {
			cout << "HashMap Obj Num is Not Enough!" << endl;
		}
	}
		break;
	case 3: {
		list<JsonData*>::iterator listItr = listJsonData.begin();
		while (count <= deleteNum && listItr != listJsonData.end())
		{
			count++;
			listJsonData.erase(listItr++);
			cout << count << " List Delete Success!" << endl;
		}
		if (count < deleteNum) {
			cout << "List Obj Num is Not Enough!" << endl;
		}
	}
		break;
	default:
		break;
	}
}
void JsonIO::updateCmp(int updateNum, const int dataModel) {
	//0:vector update
	//1:map update
	//2:hash_map update
	//3:list update
	int count = 0;
	switch (dataModel)
	{
	case 0: {
		vector<JsonData*>::iterator vecItr = vecJsonData.begin();
		while (count <= updateNum && vecItr != vecJsonData.end())
		{
			count++;
			(*vecItr)->setRandNum((*vecItr)->getRandNum() + 1);
			vecItr++;
			cout << count << " Vector Update Success!" << endl;
		}
		if (count < updateNum) {
			cout << "Vector Obj Num is Not Enough!" << endl;
		}
	}
		  break;
	case 1: {
		map<int, JsonData*>::iterator mapItr = mapJsonData.begin();
		while (count <= updateNum && mapItr != mapJsonData.end())
		{
			count++;
			(mapItr->second)->setRandNum((mapItr->second)->getRandNum() + 1);
			mapItr++;
			cout << count << " Map Update Success!" << endl;
		}
		if (count < updateNum) {
			cout << "Map Obj Num is Not Enough!" << endl;
		}
	}
		  break;
	case 2: {
		unordered_map<int, JsonData*>::iterator hashMapItr = hashMapJsonData.begin();
		while (count <= updateNum && hashMapItr != hashMapJsonData.end())
		{
			count++;
			(hashMapItr->second)->setRandNum((hashMapItr->second)->getRandNum() + 1);
			hashMapItr++;
			cout << count << " HashMap Update Success!" << endl;
		}
		if (count < updateNum) {
			cout << "HashMap Obj Num is Not Enough!" << endl;
		}
	}
		  break;
	case 3: {
		list<JsonData*>::iterator listItr = listJsonData.begin();
		while (count <= updateNum && listItr != listJsonData.end())
		{
			count++;
			(*listItr)->setRandNum((*listItr)->getRandNum() + 1);
			listItr++;
			cout << count << " List Update Success!" << endl;
		}
		if (count < updateNum) {
			cout << "List Obj Num is Not Enough!" << endl;
		}
	}
		  break;
	default:
		break;
	}
	
}
void JsonIO::findCmp(int jdObjId, const int dataModel) {
	//0:vector find
	//1:map find
	//2:hash_map find
	//3:list find
	switch (dataModel)
	{
	case 0: {
		vector<JsonData*>::iterator vecItr = vecJsonData.begin();
		for (; vecItr != vecJsonData.end(); ++vecItr) {
			if ((**vecItr) == jdObjId) {
				cout << "Vector Find " << jdObjId << " Success!" << endl;
				break;
			}
		}
		if (vecItr == vecJsonData.end()) {
			cout << "There is no objID:" << jdObjId << endl;
		}
	}
		break;
	case 1:
		//map<int, JsonData*>::iterator mapItr = mapJsonData.find(jd->getObjId());
		if (mapJsonData.find(jdObjId) != mapJsonData.end()) {
			cout << "Map Find " << jdObjId << " Success!" << endl;
		}
		else {
			cout << "There is no objID:" << jdObjId << endl;
		}
		break;
	case 2:
		//unordered_map<int, JsonData*>::iterator hashMapItr = hashMapJsonData.find(jd->getObjId());
		if (hashMapJsonData.find(jdObjId) != hashMapJsonData.end()) {
			cout << "HashMap Find " << jdObjId << " Success!" << endl;
		}
		else {
			cout << "There is no objID:" << jdObjId << endl;
		}
		break;
	case 3: {
		list<JsonData*>::iterator listItr = listJsonData.begin();
		for (; listItr != listJsonData.end(); ++listItr) {
			if ((**listItr) == jdObjId) {
				(*listItr)->setRandNum((*listItr)->getRandNum() + 1);
				cout << "List Find " << jdObjId << " Success!" << endl;
				break;
			}
		}
		if (listItr == listJsonData.end()) {
			cout << "There is no objID:" << jdObjId << endl;
		}
	}
		break;
	default:
		break;
	}
}

void JsonIO::cleanUp() {
	cout << "---------------------Vector Clean Up---------------------" << endl;
	for (vector<JsonData*>::iterator itr = vecJsonData.begin(); itr != vecJsonData.end(); ++itr) {
		delete (*itr);
	}
	cout << "---------------------Map Clean Up---------------------" << endl;
	for (map<int, JsonData*>::iterator itr = mapJsonData.begin(); itr != mapJsonData.end(); ++itr) {
		delete (itr->second);
	}
	cout << "---------------------HashMap Clean Up---------------------" << endl;
	for (unordered_map<int, JsonData*>::iterator itr = hashMapJsonData.begin(); itr != hashMapJsonData.end(); ++itr) {
		delete (itr->second);
	}
	cout << "---------------------List Clean Up---------------------" << endl;
	for (list<JsonData*>::iterator itr = listJsonData.begin(); itr != listJsonData.end(); ++itr) {
		delete (*itr);
	}
	//delete this;
	cout << "---------------------JSIO Clean Up Success!---------------------" << endl;
}