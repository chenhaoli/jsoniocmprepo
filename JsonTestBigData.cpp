﻿#include "JsonIO.h"
const string fileName = "BigJsonData.json";
const int w = 100000;
const int writeNum = w;
const int addNum = 3 * w;
const int deleteNum = 3 * w;
const int updateNum = 3 * w;
const int findNum = 3 * w;

int main()
{
 //   string temp, str;
	//stringstream ss;
 //   int year=2021, month=1, day=1, hour=0, minute=0, second=0;
	//ss << setw(4) << setfill('0') << year << "-" << setw(2) << setfill('0') << month << "-" << setw(2) << setfill('0') << day << " ";
 //   ss >> temp;
 //   str = str + temp + " ";
	//ss << setw(2) << setfill('0') << hour << ":" << setw(2) << setfill('0') << minute << ":" << setw(2) << setfill('0') << second;
 //   ss >> temp;
 //   str += temp;
 //   /*while (ss >> temp) {
 //       str = str + temp + " ";
 //   }*/
 //   cout << str << endl;
	//ss.clear();
    JsonIO* jsIO = new JsonIO();
    cout << "---------------------Write Json File---------------------" << endl;
    jsIO->writeJson(fileName, writeNum);
    cout << "---------------------Load Json File---------------------" << endl;
    jsIO->loadJson(fileName);

    //int addNum = 3 * w;
    cout << "---------------------Add Vector---------------------" << endl;
    time_t addVecStartT = time(0);
    for (int i = 0; i < addNum; ++i) {
        JsonData* jd = new JsonData();
        jsIO->addCmp(jd, 0);
        //jd->cleanUp();
    }
    time_t addVecStopT = time(0);
    cout << "---------------------Add Vector Success!---------------------" << endl;
    cout << "---------------------Add Map---------------------" << endl;
    time_t addMapStartT = time(0);
    for (int i = 0; i < addNum; ++i) {
        JsonData* jd = new JsonData();
        jsIO->addCmp(jd, 1);
        //jd->cleanUp();
    }
    time_t addMapStopT = time(0);
    cout << "---------------------Add Map Success!---------------------" << endl;
    cout << "---------------------Add HashMap---------------------" << endl;
    time_t addHashMapStartT = time(0);
    for (int i = 0; i < addNum; ++i) {
        JsonData* jd = new JsonData();
        jsIO->addCmp(jd, 2);
        //jd->cleanUp();
    }
    time_t addHashMapStopT = time(0);
    cout << "---------------------Add HashMap Success!---------------------" << endl;
    cout << "---------------------Add List---------------------" << endl;
    time_t addListStartT = time(0);
    for (int i = 0; i < addNum; ++i) {
        JsonData* jd = new JsonData();
        jsIO->addCmp(jd, 3);
        //jd->cleanUp();
    }
    time_t addListStopT = time(0);
    cout << "---------------------Add List Success!---------------------" << endl;

    //int findNum = 6 * w;
    cout << "---------------------Find Vector---------------------" << endl;
    time_t findVecStartT = time(0);
    for (int i = 0; i < findNum; ++i) {
        jsIO->findCmp(i, 0);
    }
    time_t findVecStopT = time(0);
    cout << "---------------------Find Vector Success!---------------------" << endl;
    cout << "---------------------Find Map---------------------" << endl;
    time_t findMapStartT = time(0);
    for (int i = 0; i < findNum; ++i) {
        jsIO->findCmp(i, 1);
    }
    time_t findMapStopT = time(0);
    cout << "---------------------Find Map Success!---------------------" << endl;
    cout << "---------------------Find HashMap---------------------" << endl;
    time_t findHashMapStartT = time(0);
    for (int i = 0; i < findNum; ++i) {
        jsIO->findCmp(i, 2);
    }
    time_t findHashMapStopT = time(0);
    cout << "---------------------Find HashMap Success!---------------------" << endl;
    cout << "---------------------Find List---------------------" << endl;
    time_t findListStartT = time(0);
    for (int i = 0; i < findNum; ++i) {
        jsIO->findCmp(i, 3);
    }
    time_t findListStopT = time(0);
    cout << "---------------------Find List Success!---------------------" << endl;

    //int updateNum = 3 * w;
    cout << "---------------------Update Vector---------------------" << endl;
    time_t updateVecStartT = time(0);
    jsIO->updateCmp(updateNum, 0);
    time_t updateVecStopT = time(0);
    cout << "---------------------Update Vector Success!---------------------" << endl;
    cout << "---------------------Update Map---------------------" << endl;
    time_t updateMapStartT = time(0);
    jsIO->updateCmp(updateNum, 1);
    time_t updateMapStopT = time(0);
    cout << "---------------------Update Map Success!---------------------" << endl;
    cout << "---------------------Update HashMap---------------------" << endl;
    time_t updateHashMapStartT = time(0);
    jsIO->updateCmp(updateNum, 2);
    time_t updateHashMapStopT = time(0);
    cout << "---------------------Update HashMap Success!---------------------" << endl;
    cout << "---------------------Update List---------------------" << endl;
    time_t updateListStartT = time(0);
    jsIO->updateCmp(updateNum, 3);
    time_t updateListStopT = time(0);
    cout << "---------------------Update List Success!---------------------" << endl;

    //int deleteNum = 3 * w;
    cout << "---------------------Delete Vector---------------------" << endl;
    time_t deleteVecStartT = time(0);
    jsIO->deleteCmp(deleteNum, 0);
    time_t deleteVecStopT = time(0);
    cout << "---------------------Delete Vector Success!---------------------" << endl;
    cout << "---------------------Delete Map---------------------" << endl;
    time_t deleteMapStartT = time(0);
    jsIO->deleteCmp(deleteNum, 1);
    time_t deleteMapStopT = time(0);
    cout << "---------------------Delete Map Success!---------------------" << endl;
    cout << "---------------------Delete HashMap---------------------" << endl;
    time_t deleteHashMapStartT = time(0);
    jsIO->deleteCmp(deleteNum, 2);
    time_t deleteHashMapStopT = time(0);
    cout << "---------------------Delete HashMap Success!---------------------" << endl;
    cout << "---------------------Delete List---------------------" << endl;
    time_t deleteListStartT = time(0);
    jsIO->deleteCmp(deleteNum, 3);
    time_t deleteListStopT = time(0);
    cout << "---------------------Delete List Success!---------------------" << endl;


    cout << "---------------------JSIO Clean Up---------------------" << endl;
    jsIO->cleanUp();
    delete jsIO;

    cout << "---------------------Add Time Cmp----------------------" << endl;
    cout << "Add Vector Total Time:" << addVecStopT - addVecStartT  << " Second!" << endl;
    cout << "Add Map Total Time:" << addMapStopT - addMapStartT << " Second!" << endl;
    cout << "Add HashMap Total Time:" << addHashMapStopT - addHashMapStartT << " Second!" << endl;
    cout << "Add List Total Time:" << addListStopT - addListStartT << " Second!" << endl;

    cout << "---------------------Find Time Cmp----------------------" << endl;
    cout << "Find Vector Total Time:" << findVecStopT - findVecStartT << " Second!" << endl;
    cout << "Find Map Total Time:" << findMapStopT - findMapStartT << " Second!" << endl;
    cout << "Find HashMap Total Time:" << findHashMapStopT - findHashMapStartT << " Second!" << endl;
    cout << "Find List Total Time:" << findListStopT - findListStartT << " Second!" << endl;

    cout << "---------------------Update Time Cmp----------------------" << endl;
    cout << "Update Vector Total Time:" << updateVecStopT - updateVecStartT << " Second!" << endl;
    cout << "Update Map Total Time:" << updateMapStopT - updateMapStartT << " Second!" << endl;
    cout << "Update HashMap Total Time:" << updateHashMapStopT - updateHashMapStartT << " Second!" << endl;
    cout << "Update List Total Time:" << updateListStopT - updateListStartT << " Second!" << endl;

    cout << "---------------------Delete Time Cmp----------------------" << endl;
    cout << "Delete Vector Total Time:" << deleteVecStopT - deleteVecStartT << " Second!" << endl;
    cout << "Delete Map Total Time:" << deleteMapStopT - deleteMapStartT << " Second!" << endl;
    cout << "Delete HashMap Total Time:" << deleteHashMapStopT - deleteHashMapStartT << " Second!" << endl;
    cout << "Delete List Total Time:" << deleteListStopT - deleteListStartT << " Second!" << endl;
}

